// Syncronized JS loading --> $( document ).ready() {}
$(function() {
  // weather update button click
  $('#wx-button').on('click', function(e) {
    $.ajax({
      url: "http://api.wunderground.com/api/1a0eec56fc5ead5b/geolookup/conditions/q/WA/Lynnwood.json",
      dataType: "jsonp",
      success: function(parsed_json) {
        // { your code goes here to drill down on the API data }
        var state = parsed_json['location']['state'];
        var city = parsed_json['location']['city'];
        var temp_f = parsed_json['current_observation']['temp_f'];

        var wind = parsed_json['current_observation']['wind_mph'];
        var weather = parsed_json['current_observation']['weather'];
        var humidity = parsed_json['current_observation']['relative_humidity'];
        var iconURL = parsed_json['current_observation']['icon_url'];
        var time = parsed_json['current_observation']['local_time_rfc822'];

        var humidInt = parseInt(humidity[0] + humidity[1]);
        console.log(temp_f);


         var str = "<h3 id='body'> Location: " + city + ", " + state + "</h3>";
         $('#weatherContent').append(str);

         var str = "<h3 id='body'> Updated: " + time + "</h3>";
         $('#weatherContent').append(str);

        if (temp_f >= 70){
          var str = "<p id='body'> The current temperature is: " + temp_f + " F. <span id='gogo'>Today is a GREAT day to paint!</span></p>";
        $('#weatherContent').append(str);
      }
        else{
            var str = "<p id='body'> The current temperature is: " + temp_f + " F. <span id='nono'>It is too cold to paint today :(</span></p>";
          $('#weatherContent').append(str);
        }

        if (humidInt <= 60){
            var str = "<p id='body'> The relative humidity is: " + humidity +  ". <span id='gogo'>The humidity is GREAT for painting!</span></p>";
          $('#weatherContent').append(str);
        }
        else{
            var str = "<p id='body'> Relative Humidity is: " + humidity +  ". <span id='nono'>It is too humid to paint today :(</span></p>";
          $('#weatherContent').append(str);
        }

        $('#weatherContent').append("<hr id='hrSub'>");

      }
    });
  });
});
